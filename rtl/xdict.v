/*
 Descrition: generates a dictionary of symbols for the assembler
 */

`timescale 1ns / 1ps
`include "xdefs.vh"
`include "xctrldefs.vh"
`include "xregfdefs.vh"
`include "xprogdefs.vh"
`include "xuart_wrapperdefs.vh"

module xdict;

   integer fp, i;

   initial begin

      fp = $fopen("xdict.txt","w");

      $fwrite(fp,"{\n");

      //
      // CONTROLLER
      //
      
      //immediate width       
      $fwrite(fp, "\"IMM_W\":%d,\n", `IMM_W);
  
      //delay slots
      $fwrite(fp, "\"DELAY_SLOTS\":%d,\n", `DELAY_SLOTS);

      //instruction codes
      $fwrite(fp, "\"shft\":%d,\n", `SHFT);
      $fwrite(fp, "\"add\":%d,\n", `ADD);
      $fwrite(fp, "\"addi\":%d,\n", `ADDI);
      $fwrite(fp, "\"sub\":%d,\n", `SUB);
      $fwrite(fp, "\"and\":%d,\n", `AND);
      $fwrite(fp, "\"xor\":%d,\n", `XOR);
      $fwrite(fp, "\"ldi\":%d,\n", `LDI);
      $fwrite(fp, "\"ldih\":%d,\n", `LDIH);
      $fwrite(fp, "\"rdw\":%d,\n", `RDW);
      $fwrite(fp, "\"wrw\":%d,\n", `WRW);
      $fwrite(fp, "\"rdwb\":%d,\n", `RDWB);
      $fwrite(fp, "\"wrwb\":%d,\n", `WRWB);
      $fwrite(fp, "\"beqi\":%d,\n", `BEQI);
      $fwrite(fp, "\"beq\":%d,\n", `BEQ);
      $fwrite(fp, "\"bneqi\":%d,\n", `BNEQI);
      $fwrite(fp, "\"bneq\":%d,\n", `BNEQ);

      // aliases 
      $fwrite(fp, "\"nop\":%d,\n", `ADDI);
      $fwrite(fp, "\"wrc\":%d,\n", `WRW);

      
      //
      // PROG
      //
          
      $fwrite(fp, "\"INSTR_W\":%d,\n", `INSTR_W);
      $fwrite(fp, "\"PROG_ADDR_W\":%d,\n", `PROG_ADDR_W);

  
      //
      //
      // MEMORY MAP
      //
      //

      // Controller
      $fwrite(fp, "\"RB\":%d,\n", `RB);
      $fwrite(fp, "\"RC\":%d,\n", `RC);
      
      
      // Registers
      $fwrite(fp, "\"R0\":%d,\n", `REGF_BASE+`R0);
      $fwrite(fp, "\"R1\":%d,\n", `REGF_BASE+`R1);
      $fwrite(fp, "\"R2\":%d,\n", `REGF_BASE+`R2);
      $fwrite(fp, "\"R3\":%d,\n", `REGF_BASE+`R3);
      $fwrite(fp, "\"R4\":%d,\n", `REGF_BASE+`R4);
      $fwrite(fp, "\"R5\":%d,\n", `REGF_BASE+`R5);
      $fwrite(fp, "\"R6\":%d,\n", `REGF_BASE+`R6);
      $fwrite(fp, "\"R7\":%d,\n", `REGF_BASE+`R7);
      $fwrite(fp, "\"R8\":%d,\n", `REGF_BASE+`R8);
      $fwrite(fp, "\"R9\":%d,\n", `REGF_BASE+`R9);
      $fwrite(fp, "\"R10\":%d,\n",`REGF_BASE+`R10);
      $fwrite(fp, "\"R11\":%d,\n",`REGF_BASE+`R11);
      $fwrite(fp, "\"R12\":%d,\n",`REGF_BASE+`R12);
      $fwrite(fp, "\"R13\":%d,\n",`REGF_BASE+`R13);
      $fwrite(fp, "\"R14\":%d,\n",`REGF_BASE+`R14);
      $fwrite(fp, "\"R15\":%d,\n",`REGF_BASE+`R15);

      // Program memory 
      $fwrite(fp, "\"PROG_ROM\":%d,\n",`PROG_ROM);
      $fwrite(fp, "\"PROG_ROM_ADDR_W\":%d,\n", `PROG_ROM_ADDR_W);
      $fwrite(fp, "\"PROG_RAM\":%d,\n", `PROG_RAM);
      $fwrite(fp, "\"PROG_RAM_ADDR_W\":%d,\n", `PROG_RAM_ADDR_W);
      // used as data memory
      $fwrite(fp, "\"PROG_BASE\":%d,\n", `PROG_BASE); 

      // Char print module
      $fwrite(fp, "\"CPRT_BASE\":%d,\n", `CPRT_BASE);

      // LEDS module
      $fwrite(fp, "\"LEDS_BASE\":%d,\n", `LEDS_BASE);

      // UART module
      $fwrite(fp, "\"UART_DIV_DI\":%d,\n", `UART_BASE+`UART_DIV_DI);
      $fwrite(fp, "\"UART_DIV_DO\":%d,\n", `UART_BASE+`UART_DIV_DO);
      $fwrite(fp, "\"UART_DAT_DI\":%d,\n", `UART_BASE+`UART_DAT_DI);
      $fwrite(fp, "\"UART_DAT_DO\":%d,\n", `UART_BASE+`UART_DAT_DO);
      $fwrite(fp, "\"UART_WREADY\":%d,\n", `UART_BASE+`UART_WREADY);
      $fwrite(fp, "\"UART_RVALID\":%d,\n", `UART_BASE+`UART_RVALID);

      // Finish writing dictionary
      $fwrite(fp,"}\n");
      $fclose(fp);

   end

endmodule
