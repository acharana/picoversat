/*
 *  PicoSoC - A simple example SoC using PicoRV32
 *
 *  Copyright (C) 2017  Clifford Wolf <clifford@clifford.at>
 *
 *  Permission to use, copy, modify, and/or distribute this software for any
 *  purpose with or without fee is hereby granted, provided that the above
 *  copyright notice and this permission notice appear in all copies.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

`timescale 1ns / 1ps

module xuart (
	input clk,
	input resetn,

	output ser_tx,
	input  ser_rx,

	input   [3:0] reg_div_we,
	input  [31:0] reg_div_di,
	output [31:0] reg_div_do,

	input         reg_dat_we,
	input         reg_dat_re,
	input  [7:0]  reg_dat_di,
	output [7:0]  reg_dat_do,
	output        reg_dat_wready,
	output        reg_dat_rvalid
);
	reg [31:0] cfg_divider;

	reg [3:0] recv_state;
	reg [31:0] recv_divcnt;
	reg [7:0] recv_pattern;
	reg [7:0] recv_buf_data;
	reg recv_buf_valid;

	reg [9:0] send_pattern;
	reg [3:0] send_bitcnt;
	reg [31:0] send_divcnt;
	reg send_dummy;

	assign reg_div_do = cfg_divider; // cfg_divider is, by definition, the number of clock cycles needed to receive/transmit one bit through the serial RX/TX ports
	assign reg_dat_wready = ~(/*reg_dat_we &&*/ (send_bitcnt || send_dummy)); // signals that the UART is ready to transmit a new word through the TX serial port ; wait signal negated
	assign reg_dat_rvalid = recv_buf_valid; // outputs the valid bit of the RX buffer
	assign reg_dat_do = recv_buf_valid ? recv_buf_data : 8'hFF; // if the buffer has valid received data, it's outputed. If not, the output is an all 1's sequence.

	// The following always process configures the UART's BAUD rate
	// To update the clock divider, drive the desired reg_div_we bits to 1 and the desired divider value in the UART 32-bit input reg_div_di
	// When one of the bits in reg_div_we is set to 1, the UART automatically transmits a dummy (all 1's pattern) through the TX serial port 
	always @(posedge clk) begin
		if (!resetn) begin
			cfg_divider <= 1;
		end else begin
			if (reg_div_we[0]) cfg_divider[ 7: 0] <= reg_div_di[ 7: 0];
			if (reg_div_we[1]) cfg_divider[15: 8] <= reg_div_di[15: 8];
			if (reg_div_we[2]) cfg_divider[23:16] <= reg_div_di[23:16];
			if (reg_div_we[3]) cfg_divider[31:24] <= reg_div_di[31:24];
		end
	end

	/****************	
	 * Receiving RX *
	 ****************/

	always @(posedge clk) begin
		if (!resetn) begin // reset state
			recv_state <= 0;
			recv_divcnt <= 0;
			recv_pattern <= 0;
			recv_buf_data <= 0;
			recv_buf_valid <= 0;
		end else begin
			recv_divcnt <= recv_divcnt + 1; // clock (division) counter
			if (reg_dat_re) // when a new receive is in order, the data in the buffer becames invalid
				recv_buf_valid <= 0;
			case (recv_state)
				0: begin // init state. Afterwards, proceeds to wait state
					if (!ser_rx)
						recv_state <= 1;
					recv_divcnt <= 0;
				end
				1: begin // wait state: wait 2*cfg_dividers cycles. Afterwards, proceeds to the receiving states (recv_state = 2 through 9)
					if (2*recv_divcnt > cfg_divider) begin
						recv_state <= 2;
						recv_divcnt <= 0;
					end
				end
				10: begin // after states 2 through 9, update buffer with received data, set valid to 1 and return to init state 
					if (recv_divcnt > cfg_divider) begin
						recv_buf_data <= recv_pattern;
						recv_buf_valid <= 1;
						recv_state <= 0;
					end
				end
				default: begin
					if (recv_divcnt > cfg_divider) begin // receive the 8 bits (1 bit per state, recv_state = 2 through 9) 
						recv_pattern <= {ser_rx, recv_pattern[7:1]};
						recv_state <= recv_state + 4'b1;
						recv_divcnt <= 0;
					end
				end
			endcase
		end
	end

	assign ser_tx = send_pattern[0]; // transmited bit through the serial TX port is the LSB of the send_pattern

	/*******************	
	 * Transmission TX *
	 *******************/

	always @(posedge clk) begin 
		if (reg_div_we) // control configuration for when the clock divider is being updated, so that tx sends a dummy (all 1's pattern)
			send_dummy <= 1;
		send_divcnt <= send_divcnt + 1;	// clock (division) counter			
		if (!resetn) begin // reset configuration state
			send_pattern <= 10'h3ff;
			send_bitcnt <= 0;
			send_divcnt <= 0;
			send_dummy <= 1;
		end else begin
			if (send_dummy && !send_bitcnt) begin // dummy configuration (all 1's pattern) while the clock divider is being updated
				send_pattern <= 10'h3ff;
				send_bitcnt <= 15;
				send_divcnt <= 0;
				send_dummy <= 0;
			end else
			
			if (reg_dat_we && !send_bitcnt) begin                   // send_pattern configuration
				send_pattern <= {1'b1, reg_dat_di[7:0], 1'b0};  // NOTE THAT THE send_dummy HAS PRIORITY OVER reg_dat_we, 
				send_bitcnt <= 10;                              // SO BE SURE TO PUT reg_div_we = 0 TO ALLOW THE
				send_divcnt <= 0;                               // send_pattern TRANSMISSION
			end else
			if (send_divcnt > cfg_divider && send_bitcnt) begin // serial transmission (either of the dummy or the send_pattern) occurs
				send_pattern <= {1'b1, send_pattern[9:1]};
				send_bitcnt <= send_bitcnt - 4'b1;
				send_divcnt <= 0;
			end
		end
	end
endmodule
