//
// UART DEFINES
//

// UART ADDRESS WIDTH
`define UART_ADDR_W 3 //2**3 = 8 registers (although only 6 are used)


// MEMORY MAP
`define UART_DIV_DI 0 // 32: UART Clock Divider Input  (xuart.v's reg_div_di)
`define UART_DIV_DO 1 // 33: UART Clock Divider Output (xuart.v's reg_div_do)
`define UART_DAT_DI 2 // 34: UART Data Input           (xuart.v's reg_dat_di)
`define UART_DAT_DO 3 // 35: UART Data Output          (xuart.v's reg_dat_do)
`define UART_WREADY 4 // 36: UART Write Ready          (xuart.v's reg_dat_wready)
`define UART_RVALID 5 // 37: UART Read Valid           (xuart.v's reg_dat_rvalid)
