`timescale 1ns / 1ps
`include "xdefs.vh"
`include "xuart_wrapperdefs.vh"

module xuart_wrapper (
   input                       clk,
   input                       rst,
   
   // serial interface
   output                      ser_tx,
   input                       ser_rx,
   
   // data interface
   input                       sel,
   input                       we,
   input  [`UART_ADDR_W-1:0]   addr,
   input  [`DATA_W-1:0]        data_in,
   output [`DATA_W-1:0]        data_out
   );
	
   // select signals
   wire                        div_di_sel;
   wire                        div_do_sel;
   wire                        dat_di_sel;
   wire                        dat_do_sel;
   wire                        wready_sel;
   wire                        rvalid_sel;

   // UART output data buses/wires
   wire [`DATA_W-1:0]          div_do;
   wire [7:0]                  dat_do;
   wire                        wready;
   wire                        rvalid;

   // computes the select signals (some of which are also the UART's we and re signals)
   assign div_di_sel = sel && (addr == `UART_DIV_DI) && we;    // write
   assign div_do_sel = sel && (addr == `UART_DIV_DO) && (~we); // read
   assign dat_di_sel = sel && (addr == `UART_DAT_DI) && we;    // write
   assign dat_do_sel = sel && (addr == `UART_DAT_DO) && (~we); // read
   assign wready_sel = sel && (addr == `UART_WREADY) && (~we); // read;
   assign rvalid_sel = sel && (addr == `UART_RVALID) && (~we); // read;
   
   // multiplexer to select which data to send to the wrapper's output
   assign data_out = div_do_sel ? div_do                           :
                     dat_do_sel ? { {(`DATA_W-8){1'b0}} , dat_do } :
                     wready_sel ? { {(`DATA_W-1){1'b0}} , wready } :
                     rvalid_sel ? { {(`DATA_W-1){1'b0}} , rvalid } :
                     /* else */   `DATA_W'd0                       ;

   // instatiation of the UART
   xuart uart (
      .clk(clk),
      .resetn(~rst),
      
      .ser_tx(ser_tx),
      .ser_rx(ser_rx),

      .reg_div_we({(`DATA_W/8){div_di_sel}}),
      .reg_div_di(data_in),
      .reg_div_do(div_do),
      
      .reg_dat_we(dat_di_sel),
      .reg_dat_re(dat_do_sel),
      .reg_dat_di(data_in[7:0]),
      .reg_dat_do(dat_do),
      .reg_dat_wready(wready),
      .reg_dat_rvalid(rvalid)
      );
   
endmodule
