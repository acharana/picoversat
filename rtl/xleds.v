`timescale 1ns / 1ps
`include "xdefs.vh"
`include "xledsdefs.vh"

module xleds (
		input 	    clk,
		input 	    sel,
		input [`NUMBER_OF_LEDS-1:0] data_in,
		output reg [`NUMBER_OF_LEDS-1:0] data_out
		);

 always @(posedge clk)
   if(sel)
     data_out = data_in;

endmodule
